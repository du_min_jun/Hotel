/* JS Document */

/******************************

[Table of Contents]

1. Vars and Inits
2. Set Header
3. Menu
4. Init Magic


******************************/

$(document).ready(function()
{
	"use strict";

	/* 

	1. Vars and Inits

	*/

	var header = $('.header');
	var menu = $('.menu');
	var menuActive = false;
	var ctrl = new ScrollMagic.Controller();

	setHeader();

	$(window).on('resize', function()
	{
		setHeader();
	});

	$(document).on('scroll', function()
	{
		setHeader();
	});


	initMagic();

	/* 

	2. Set Header

	*/

	function setHeader()
	{
		if($(window).scrollTop() > 91)
		{
			header.addClass('scrolled');
		}
		else
		{
			header.removeClass('scrolled');
		}
	}

	/* 

	3. Menu

	*/

	$( "span.menu" ).click(function() {
		$( ".navbar" ).slideToggle( "slow", function() {
			// Animation complete.
		});
	});

	/*

	4. Init Magic

	*/

	function initMagic()
	{
		if($('.magic_up').length)
		{
			var magicUp = $('.magic_up');
	    	magicUp.each(function()
	    	{
	    		var ele = this;
	    		var smScene = new ScrollMagic.Scene(
		    	{
		    		triggerElement:ele,
		    		triggerHook:'onEnter',
		    		offset:-200,
		    		reverse:false
		    	})
		    	.setTween(TweenMax.from(ele, 1, {y:200, autoAlpha:0, ease: Circ.easeOut, delay:0.3}))
		    	.addTo(ctrl);	
	    	});
		}
	}

});